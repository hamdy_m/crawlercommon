'''
Created on Dec 8, 2014
@author: Mohammed Hamdy
'''

from spidersite.spidersitex.models import FirmenResultsModel, WLWResultsModel, WhoisResultsModel

def export_all_models(results_models):
  
  for result_model in results_models:
    result_model.objects.update_stats() # for the details page
    result_model.objects.export_data() # for the download
    
if __name__ == "__main__":
  export_all_models([FirmenResultsModel, WLWResultsModel, WhoisResultsModel])
  