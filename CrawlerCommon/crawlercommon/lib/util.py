'''
Created on Dec 6, 2014
@author: Mohammed Hamdy
'''
import os.path as path
from threading import Thread
from django.contrib import messages
from crawlercommon import DOWNLOAD_DIRECTORY

def timeFromSeconds(seconds):
  time_taken_mins = int(seconds / 60)
  time_taken_hours = int(time_taken_mins / 60)
  # then minutes and seconds
  mins_seconds = seconds - time_taken_hours * 60 * 60
  mins = int(mins_seconds / 60)
  seconds = int(mins_seconds - (mins * 60))
  return (time_taken_hours, mins, seconds)

def getExportFileForSpider(spiderName, baseDir=None):
  if baseDir is None:
    baseDir = DOWNLOAD_DIRECTORY
  return path.join(baseDir, spiderName, "records.csv")

def populate_messages(request, operation_str, return_code):
  if return_code == 0:
    messages.success(request, "Operation '{}' has succeeded!".format(operation_str))
  else:
    messages.warning(request, "Operation '{}' has failed!. Please check the log".format(operation_str))
    
def run_in_thread(f, *args, **kwargs):
  t = Thread(target=f, args=args, kwargs=kwargs)
  t.start()