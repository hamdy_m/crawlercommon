'''
Created on Nov 29, 2014
@author: Mohammed Hamdy
'''

from threading import Timer
from visualscrape.lib.event_handler import IEventHandler
from crawlercommon.lib.dump_manager import DumpManager
from visualscrape.lib.signal import SpiderClosed

class DumpingHandler(IEventHandler):
  
  def __init__(self, dumpDir):
    self._queue_check_interval = 3
    self._queue_check_looper = Timer(15, self.check_queues)
    self._queue_check_looper.start()
    self.event_queue = None
    self.data_queue = None
    self._dump_manager = DumpManager(dumpDir)
    
  def set_event_queue(self, queue):
    self.event_queue = queue
    
  def set_data_queue(self, queue):
    self.data_queue = queue
    
  def check_queues(self):
    finished = False
    if self.data_queue is None or self.event_queue is None:
      pass
    else:
      while not self.data_queue.empty():
        new_item = self.data_queue.get(block=False, timeout=0)
        self._dump_manager.dump_item(new_item)
      while not self.event_queue.empty():
        new_event = self.event_queue.get(block=False, timeout=0)
        if isinstance(new_event, SpiderClosed):
          self._dump_manager.finalize_item_dump()
          finished = True
    if not finished:
      Timer(self._queue_check_interval, self.check_queues).start()
      