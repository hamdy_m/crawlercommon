'''
Created on Dec 7, 2014
@author: Mohammed Hamdy
'''

import signal, logging
from subprocess32 import call
from spidersite.spidersitex.models import SpiderInfo
from spidersite.lib.util import getSiteLogDir
from crawlercommon.lib.loggers import CommandLogger


class SignalMixin(object):
  
  """
  Adds signal handling capabilities to a server by making it responsive to 
  specific signals like SIGTERM and SIGHUP. These allow the server
  to be controlled from it's PID
  Where should 
  """
  
  def __init__(self, server):
    self.server = server
    signal.signal(signal.SIGTERM, self.stop_spider)
    signal.signal(signal.SIGHUP, self.pause_or_resume_spider)
    
  def stop_spider(self, signum, frame):
    self.server.handleStop()
  
  def pause_or_resume_spider(self, signum, frame):
    self.server.handlePauseOrResume()
  
class SpiderController(object):
  """
  Given a spider id from the SpiderInfo database, objects of this class can
  start, stop, pause and resume the spider in question
  """
  
  def __init__(self, username):
    self.kill_command = ["kill", "-SIGTERM"]
    self.pause_resume_command = ["kill", "-SIGHUP"]
    self.logger = CommandLogger(getSiteLogDir())
    self.success_template = "user '%s' issued {command} for {spidername}. command succeeded" % username
    self.failure_template = "user '%s' issued {command} for {spidername}. command failed with return code {code}" % username
    
  def start_spider(self, spiderId):
    spider_record = self._get_spider_record(spiderId)
    spidername = spider_record.name
    spider_startup_command = spider_record.startup_command
    return_code = call(spider_startup_command.split(' '))
    self._handle_return_code(return_code, command="start", spidername=spidername)
    return return_code
    
  def pause_spider(self, spiderId):
    command = self.pause_resume_command[:]
    spider_record = self._get_spider_record(spiderId)
    command.append(str(spider_record.process_id))
    return_code = call(command)
    self._handle_return_code(return_code, "pause", spider_record.name)
    return return_code
    
  def resume_spider(self, spiderId):
    spider_record = self._get_spider_record(spiderId)
    command = self.pause_resume_command[:]
    command.append(str(spider_record.process_id))
    return_code = call(command)
    self._handle_return_code(return_code, "resume", spider_record.name)
    return return_code
    
  def shutdown_spider(self, spiderId):
    spider_record = self._get_spider_record(spiderId)
    command = self.kill_command[:]
    command.append(str(spider_record.process_id))
    return_code = call(command)
    self._handle_return_code(return_code, "shutdown_if_no_clients", spider_record.name)
    return return_code
  
  def _get_spider_record(self, spiderId):
    return SpiderInfo.objects.get(pk=spiderId)
  
  def _handle_return_code(self, returncode, command, spidername):
    # check the return code and log a message accordingly
    if returncode == 0:
      message = self.success_template.format(command=command, spidername=spidername)
    else:
      message = self.failure_template.format(command=command, spidername=spidername, code=returncode)
    self.logger.log(message, logging.INFO)
    
if __name__ == "__main__":
  from argparse import ArgumentParser
  parser = ArgumentParser()
  action_group = parser.add_mutually_exclusive_group()
  action_group.add_argument("-s", "--start", action="store_true", help="start the spider with database id")
  action_group.add_argument("-p", "--pause", action="store_true", help="send a pause signal the spider with database id")
  action_group.add_argument("-r", "--resume", action="store_true", help="send a resume signal the spider with database id")
  action_group.add_argument("-sd", "--shutdown_if_no_clients", action="store_true", help="send a shutdown_if_no_clients signal to the spider with database id")
  parser.add_argument("id", type=int, help="the database id of the spider")
  arguments = parser.parse_args()
  controller = SpiderController("test")
  if arguments.start:
    controller.start_spider(arguments.id)
  elif arguments.pause:
    controller.pause_spider(arguments.id)
  elif arguments.resume:
    controller.resume_spider(arguments.id)
  elif arguments.shutdown_if_no_clients:
    controller.shutdown_spider(arguments.id)
  