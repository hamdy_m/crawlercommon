'''
Created on Nov 23, 2014
@author: Mohammed Hamdy
'''

import logging
from logging.handlers import TimedRotatingFileHandler
import os.path as path, os

class BaseLogger(object):
  
  def __init__(self, logDir):
    self.log_dir = logDir
    if not path.exists(logDir):
      os.mkdir(logDir)
      
class ClientLogger(BaseLogger):
  
  def __init__(self, *args, **kwargs):
    super(ClientLogger, self).__init__(*args, **kwargs)
    self.setupLog()

  def setupLog(self):
    client_logger = logging.getLogger(name="Client")
    client_logger.setLevel(logging.DEBUG)
    log_file_path = path.join(self.log_dir, "client_log.log")
    client_handler = TimedRotatingFileHandler(log_file_path,
                        when='H', interval=24, backupCount=2)
    client_format = logging.Formatter('%(levelname)s @ [%(asctime)s] :: %(message)s',
                                       datefmt="%Y-%m-%d %H:%M:%S")
    
    client_handler.setFormatter(client_format)
    client_logger.addHandler(client_handler)
    self.logger = client_logger
    
  def log(self, message, level):
    self.logger.log(level, message)

class ServerLogger(BaseLogger):
  
  def __init__(self, *args, **kwargs):
    super(ServerLogger, self).__init__(*args, **kwargs)
    self.setupLog()
    
  def setupLog(self):
    server_logger = logging.getLogger(name="Server")
    server_logger.setLevel(logging.DEBUG)
    self.log_file_path = path.join(self.log_dir, "server_log.log")
    server_handler = TimedRotatingFileHandler(self.log_file_path,
                        when='H', interval=24, backupCount=2)
    server_format = logging.Formatter('%(levelname)s @ [%(asctime)s] :: %(message)s',
                                       datefmt="%Y-%m-%d %H:%M:%S")
    
    server_handler.setFormatter(server_format)
    server_logger.addHandler(server_handler)
    self.logger = server_logger
    
  def log(self, message, level):
    self.logger.log(level, message)
    
  def getLogFilePath(self):
    return self.log_file_path

class CommandLogger(BaseLogger):
  
  def __init__(self, *args, **kwargs):
    # username is the registered name of the user and it appears in the logs
    super(CommandLogger, self).__init__(*args, **kwargs)
    self.setupLog()
      
  def setupLog(self):
    command_logger = logging.getLogger(name="Command")
    command_logger.setLevel(logging.DEBUG)
    log_file_path = path.join(self.log_dir, "control_log.log")
    command_handler = TimedRotatingFileHandler(log_file_path,
                        when='H', interval=24, backupCount=2)
    command_format = logging.Formatter('%(levelname)s @ [%(asctime)s] :: %(message)s',
                                       datefmt="%Y-%m-%d %H:%M:%S")
    
    command_handler.setFormatter(command_format)
    command_logger.addHandler(command_handler)
    self.logger = command_logger
    
  def log(self, message, level):
    self.logger.log(level, message)
