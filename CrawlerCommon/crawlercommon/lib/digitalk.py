"""
Created on Nov 16, 2014
@author: Mohammed Hamdy
@description : I'll always make changes to the server and one client manually.
  Changes to the rest of the clients will be automated here...
"""

from __future__ import print_function
import digitalocean, time, sys
from crawlercommon.lib.util import run_in_thread
READY_CLIENT_NAME = "ReadyClient"
CLONE_CLIENT_NAME = "Client"

def cloneReadyClient(cloneCount=70):
  # 3024869 is the id for WHoisClient
  manager = getManager()
  # create a snapshot of the known ready client
  client_snapshot_name = "latest_client_snapshot"
  # first, delete the snapshot with the same name
  for snapshot in manager.get_all_images():
    if snapshot.name == client_snapshot_name:
      snapshot.destroy()
      print("Destroyed {} snapshot".format(snapshot.name))
      break
  
  ready_client = getClientByName(READY_CLIENT_NAME, manager)
  if ready_client.status == "active":
    shutdown_action_resource =  ready_client.shutdown()
    waitForActionResource(manager, shutdown_action_resource)
    time.sleep(30) # ensure a proper shutdown. it always fails here even with the waitForActionResource
    print("Client {} shutdown".format(ready_client.name))
  take_snapshot_action_resource = ready_client.take_snapshot(client_snapshot_name)
  waitForActionResource(manager, take_snapshot_action_resource)
  print("Created {} snapshot".format(ready_client.name))
  # get the new snapshot id
  for snapshot in manager.get_all_images():
    if snapshot.name == client_snapshot_name:
      latest_snapshot = snapshot
      break
  else:
    print("Couldn't find snapshot with name : {}. exiting", file=sys.stderr)
    sys.exit(1)
    
  # delete all client clone droplets
  destroyed_count = destroyAllClients()
  print("Destroyed {} clients".format(destroyed_count))
  
  for _ in range(cloneCount):
    new_droplet = digitalocean.Droplet(token=manager.token,
                    name=CLONE_CLIENT_NAME,
                    region='nyc3', image=latest_snapshot.id, 
                    size_slug='512mb', backups=False)
    new_droplet.create()
  print("Created {} droplets with image {}".format(cloneCount, latest_snapshot.name))
  print("Done!")
  
  
def getClientByName(clientName, manager):
  droplets = manager.get_all_droplets()
  for droplet in droplets:
    if droplet.name == clientName:
      return droplet
  else:
    return None
  
def waitForActionResource(manager, actionResource):
  action_id = actionResource["action"]["id"]
  action = manager.get_action(action_id)
  while action.status != "completed":
    time.sleep(1)
    action = manager.get_action(action_id)
    
def rebootAllClients():
  manager = getManager()
  droplets = manager.get_all_droplets()
  for droplet in droplets:
    if droplet.name == CLONE_CLIENT_NAME or droplet.name == READY_CLIENT_NAME:
      reboot_resource = droplet.reboot()
      waitForActionResource(manager, reboot_resource)
  return len(droplets)
    
def destroyAllClients():
  manager = getManager()
  droplets = manager.get_all_droplets()
  destroy_results = []
  for droplet in droplets:
    if droplet.name == CLONE_CLIENT_NAME:
      destroy_results.append(droplet.destroy())
  return len(destroy_results)
    
def getClientCount():
  manager = getManager()
  droplets = manager.get_all_droplets()
  client_count = 0
  for droplet in droplets:
    if droplet.name == READY_CLIENT_NAME or droplet.name == CLONE_CLIENT_NAME:
      client_count += 1
  return client_count
    
def getManager():
  return digitalocean.Manager(token="d9f0b450e393d99c17731eef7cd4743c14b09a0dba7aab4e506504df16972638")
    
if __name__ == "__main__":
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("-d", "--destroy", help="Destroy all client nodes named {}".format(CLONE_CLIENT_NAME),
                      action="store_true")
  parser.add_argument("-c", "--clone", help="Make clones of client named {}".format(READY_CLIENT_NAME),
                      type=int)
  parser.add_argument("-r", "--reboot", help="Reboot all clients named {}".format(READY_CLIENT_NAME),
                      action="store_true")
  args = parser.parse_args()
  if args.destroy:
    run_in_thread(destroyAllClients)
  elif args.clone:
    run_in_thread(cloneReadyClient, args.clone)
  elif args.reboot:
    run_in_thread(rebootAllClients)