'''
Created on Nov 29, 2014
@author: Mohammed Hamdy
'''

import os.path as path, cPickle as pickle, os

class DumpManager(object):
  
  def __init__(self, dumpDir):
    self._item_dump_path = path.join(dumpDir, "item_dump.bin")
    self._clean_item_dump()
    self._job_dump_path = path.join(dumpDir, "job_dump.bin")
    self._dump_finished_mark_path = path.join(dumpDir, "dump_finished.mark")
  
  def next_job_batch(self):
    # called from url generator
    with open(self._job_dump_path, "rb") as job_dump:
      self._job_dump_available = False
      return pickle.load(job_dump)
    return []
  
  def finalize_item_dump(self):
    # called from item handler when spider is closed
    self._set_item_dump_finished(True)
  
  def dump_item(self, item):
    # this will dump a new item from the spider handler
    with open(self._item_dump_path, "ab") as item_dump:
      pickle.dump(item, item_dump)
      
  def get_dumped_items(self):
    if self._is_item_dump_finished():
      items = []
      with open(self._item_dump_path, "rb") as item_dump:
        while True:
          try:
            items.append(pickle.load(item_dump))
          except EOFError:
            self._set_item_dump_finished(False)
            return items
    else:
      return None
  
  def dump_jobs(self, jobList):
    with open(self._job_dump_path, "wb") as job_dump:
      pickle.dump(jobList, job_dump)
      self._job_dump_available = True
    
  def _clean_item_dump(self):
    with open(self._item_dump_path, "wb") as _:
      pass

  def _set_item_dump_finished(self, state):
    if state is True:
      with open(self._dump_finished_mark_path, "wb") as _:
        pass
    else:
      os.remove(self._dump_finished_mark_path)
      
  def _is_item_dump_finished(self):
    return path.exists(self._dump_finished_mark_path)
  