#!/bin/bash

# downloaded from : https://www.dropbox.com/s/shiynz9j1m6t4lm/setup.tar.gz?dl=0
# USAGE : pre_crawl.sh [client|server] [SERVER_ADDRESS] [CLIENT_NAME]
# pass the second and third arguments only for clients

# fix "Setting locale failed" error
export LANGUAGE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
echo 'LANGUAGE="en_US.UTF-8"' >> /etc/default/locale
echo 'LC_ALL="en_US.UTF-8"' >> /etc/default/locale
locale-gen en_US

echo adding new user account...
user_type=$1
new_user=0
if [ $user_type = client ]; then
  new_user=whoisclient
else
  new_user=whoisserver
fi

echo "enter information for user [$new_user]"
adduser $new_user

echo 'make yourself sudoer'
echo 'see : https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-14-04'
echo 'press enter to continue'
read
visudo

echo "configure ssh port to 45811, disable root and AllowUser [$new_user]"
echo 'see : https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-14-04'
echo 'press enter to continue'
read
nano /etc/ssh/sshd_config
service ssh restart

echo 'installing Python psycopg2 dependencies...'
apt-get install libpq-dev python-dev
echo

echo 'installing scrapy dependencies...'
apt-get install libxslt-dev
echo 

echo 'installing required Python packages...'
echo
apt-get install python-pip
pip install -r requirements.txt
echo

echo 'installing git...'
apt-get install git

echo 'configuring git...'
git config --global user.name "Mohammed Hamdy"
git config --global user.email "mohammed.hamdy777@gmail.com"

user_git_dir=/home/$new_user/git
echo creating git folder at $user_git_dir ...
mkdir -p $user_git_dir

echo cloning whoiscrawler repository...
pushd $user_git_dir
git clone https://hamdy_m@bitbucket.org/Ploetzi/whoiscrawler.git

echo "adding a startup entry for [$user_type]"
crawler_dir=$user_git_dir/whoiscrawler/WhoisCrawler
chown -R $new_user $crawler_dir 
chmod +x -R $crawler_dir

protocol_dir=$crawler_dir/protocol

# this is to allows twistd to discover our plugins/services
echo "Adding protocol directory [$protocol_dir] to PYTHONPATH..."
echo "PYTHONPATH=$protocol_dir:$crawler_dir" >> /etc/environment

echo "Creating run directories for twistd log and pid files..."
$server_rundir=$crawler_dir/rundirs/client
$client_rundir=$crawler_dir/rundirs/server
mkdir -p $server_rundir
mkdir -p $client_rundir

log_dir=$crawler_dir/logs
if [ $user_type = client ]; then

    echo 'installing whois...'
    apt-get install whois
    echo
    
    echo 'installing tor...'
    apt-get install tor
    
    torrc_path=/etc/tor/torrc
    echo ControlPort 9051 >> $torrc_path
    echo CookieAuthentication 1 >> $torrc_path
    service tor restart
    
    #this should be determined by AUTHENTICATing and running PROTOCOLINFO on tor command
    auth_cookie_path=/var/run/tor/control.authcookie
    hexdump -e '32/1 "%02x""\n"' $auth_cookie_path
    
    tor_username=$new_user
    usermod -a -G debian-tor $tor_username
    echo "added [$new_user] to debian-tor group"
    
    echo 'downloading torsocks...'
    torsocks_output_file=torsocks.tar.gz
    torsocks_folder=torsocks-1.2
    wget https://torsocks.googlecode.com/files/torsocks-1.2.tar.gz -O $torsocks_output_file
    echo extracting torsocks...
    tar -xf $torsocks_output_file
    pushd $torsocks_folder
    echo 'installing torsocks...'
    configure
    make
    make install
    popd
    rm -rf $torsocks_folder
    rm $torsocks_output_file

    echo "/usr/bin/python --rundir=$client_rundir whoisclient -a $2 -n $3" > /etc/rc.local
    
    apt-get install privoxy
    echo "add this to the end of /etc/privoxy/config : 'forward-socks5t  /   127.0.0.1:9050 .'"
    echo "press enter to continue"
    read
    nano /etc/privoxy/config
    service tor restart
    service privoxy restart
    mkdir -p $wlw_client_dir/dump_arena
else
  echo 'installing postgresql'
  apt-get update
  apt-get install postgresql postgresql-contrib
  echo
  
  startup_script=$protocol_dir/crawl_server.py
  # run database script as main to move german.csv to database
  /usr/bin/python $crawler_dir/database/connection.py
  
  site_dir=$crawler_dir/whoissite

  #see: http://uwsgi-docs.readthedocs.org/en/latest/tutorials/Django_and_nginx.html
  echo "Installing nginx..."
  apt-get install nginx
  sudo /etc/init.d/nginx start
  mkdir -p /var/www/spidersite/media
  chmod 664 /var/www/spidersite/media
  mkdir -p /var/www/spidersite/download
  chmod 664 /var/www/spidersite/download
  ln -s $site_dir/whoissite_nginx.conf /etc/nginx/sites-enabled
  
  echo "Don't forget to edit twistd to include the plugin directory!"
  echo "/usr/bin/twistd --logfile=$log_dir/twisted_server.log --pidfile=$server_rundir/twistd.pid whoisserver" > /etc/rc.local
  echo "/usr/bin/twistd --logfile=$log_dir/twisted_client.log --pidfile=$client_dir/twistd.pid whoisclient -a $2 -n localhost" >> /etc/rc.local
  
  echo 'Opening tcp port 8841 for clients...'
  iptables -A INPUT -p tcp --dport 8841 -j ACCEPT
  echo "Opening port 80 for web connections..."
  iptables -A INPUT -p tcp --dport 80 -j ACCEPT
  pushd $site_dir # because uwsgi doesn't support directories to modules
  echo "/usr/local/bin/uwsgi $site_dir/uwsgi.ini" >> /etc/rc.local
  popd
  echo "exit 0" >> /etc/rc.local

  echo "Sechduling summary export for the home page..."
  echo "Enter : 0,10,20,30,40,50 * * * * python /home/whoisserver/git/whoiscrawler/WhoisCrawler/database/export_summary.py"
  echo "copy and press enter to continue"
  read
  crontab -e
fi
popd

echo rebooting to apply changes, press enter to continue...
read
shutdown -r now

