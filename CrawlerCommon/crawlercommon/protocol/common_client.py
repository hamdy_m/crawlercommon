'''
Created on Dec 7, 2014
@author: Mohammed Hamdy
'''

import logging, sys, os.path as path
from subprocess import Popen
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.protocols.basic import LineReceiver
from twisted.internet import reactor, task
from firmen_crawler.protocol.common import ClientToServerSignals, ServerToClientSignals
from crawlercommon.lib.loggers import ClientLogger
from crawlercommon.lib.dump_manager import DumpManager


class BaseClientProtocol(LineReceiver):
  
  def connectionMade(self):
    # identify myself to server
    self.logger.log("connected to server", logging.INFO)
    self.sendLine(ClientToServerSignals.hello())
    # make client ready only if the last launched spider is done, i.e, avoid stale spiders
    if self.factory.last_spider_done:
      self.sendLine(ClientToServerSignals.makeReadyMessage())
    
  def lineReceived(self, line):
    if ServerToClientSignals.isJobMessage(line):
      crawl_url_subs = ServerToClientSignals.jobsFromMessage(line)
      self.logger.log("jobs ({} : {}) arrived from server".format(crawl_url_subs[0], crawl_url_subs[-1]),
                    logging.INFO)
      self.factory.jobsArrived(crawl_url_subs)
      
    elif ServerToClientSignals.isShutdownMessage(line):
      self.logger.log("got shutdown message from server. bye!", 
                    logging.INFO)
      self.transport.loseConnection()
      reactor.stop()
      
  def connectionLost(self, reason):
    LineReceiver.connectionLost(self, reason=reason)
    self.logger.log(reason, logging.ERROR)
  
  def itemReceived(self, item):
    item_message = self.messageFromItem(item)
    # encode to binary before sending
    item_message = item_message.encode("utf-8", errors="ignore")
    self.sendLine(item_message)
    
  def spiderFinished(self):
    self.logger.log("finished crawling job. requesting more...", logging.INFO)
    self.sendLine(ClientToServerSignals.makeReadyMessage())
    
  def messageFromItem(self, item):
    raise NotImplemented
    

class BaseProtocolFactory(ReconnectingClientFactory):
  
  def __init__(self, baseDir, maxDelay=60):
    # baseDir is used to obtains 2 subdirectories for logs and client dump
    self.logger = ClientLogger(path.join(baseDir, "logs"))
    self.maxDelay = maxDelay
    self.data_queue = None
    self.event_queue = None
    self.client = None
    self.last_spider_done = True # useful when a client disconnects and it's spider is still alive
    self._dump_manager = DumpManager(path.join(baseDir, "dump_arena"))
    self._item_check_looper = task.LoopingCall(self.check_item_dump)
    self._item_check_looper.start(5, now=False)
    self.logger.log("client started", logging.INFO)
    
  def buildProtocol(self, addr):
    # http://twistedmatrix.com/trac/browser/tags/releases/twisted-14.0.2/twisted/internet/protocol.py#L320
    ReconnectingClientFactory.buildProtocol(self, addr)
    self.resetDelay()
  
  def check_item_dump(self):
    dumped_items = self._dump_manager.get_dumped_items()
    if dumped_items is not None:
      for item in dumped_items:
        self.client.itemReceived(item)
      self.client.spiderFinished()
      self.last_spider_done = True
        
  def jobsArrived(self, jobs):
    self._dump_manager.dump_jobs(jobs)
    Popen([sys.executable, self.spider_command]) # run the spider from the active vm, if any
    self.logger.log("spider launched", logging.INFO)
    self.last_spider_done = False
