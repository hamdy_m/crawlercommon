'''
Created on Nov 23, 2014
@author: Mohammed Hamdy
'''

class BaseClientToServerSignals(object):
  
  
  @classmethod
  def hello(cls):
    return "Hello!"
  
  @classmethod
  def isHelloMessage(cls, message):
    return message == "Hello!"
  
  @classmethod
  def messageFromItem(cls, item):
    raise NotImplemented
  
  @classmethod
  def itemFromMessage(cls, message):
    raise NotImplemented
  
  @classmethod
  def isItemMessage(cls, message):
    return message.startswith("Item:")
  
  @classmethod
  def makeReadyMessage(cls):
    return "Ready"
  
  @classmethod
  def isReadyMessage(cls, message):
    return message.endswith("Ready")


class BaseServerToClientSignals(object):
  
  @classmethod
  def makeJobMessage(cls, jobList):
    return "Jobs:" + ','.join([str(i) for i in jobList]) # because twisted refrains on unicode 
  
  @classmethod
  def isJobMessage(cls, message):
    return message.startswith("Jobs:")
  
  @classmethod
  def jobsFromMessage(cls, message):
    suffix_str = message.split("Jobs:")[1]
    return suffix_str.split(',')
  
  @classmethod
  def makeShutdownMessage(cls):
    return "Shutdown"
  
  @classmethod
  def isShutdownMessage(cls, message):
    return message == "Shutdown"
