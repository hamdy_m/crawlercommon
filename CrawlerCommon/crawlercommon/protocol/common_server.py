'''
Created on Dec 7, 2014
@author: Mohammed Hamdy
'''

from __future__ import division
import logging, os.path as path, sys, os
from time import sleep
from random import randrange
from twisted.internet import reactor
from twisted.protocols import basic
from twisted.internet.protocol import Factory
from crawlercommon.lib.loggers import ServerLogger
from crawlercommon.protocol.common import BaseClientToServerSignals, BaseServerToClientSignals
from crawlercommon.lib.signal_manager import SignalMixin
from spidersite.spidersitex.models import SpiderInfo
from crawlercommon.lib.util import getExportFileForSpider


class BaseServerProtocol(basic.LineReceiver):
  
  def lineReceived(self, line):
    if BaseClientToServerSignals.isHelloMessage(line):
      self.factory.addClient(self)
    elif BaseClientToServerSignals.isReadyMessage(line):
      self.factory.setClientReady(self)
    elif BaseClientToServerSignals.isItemMessage(line): # client sent a new parsed record
      self.factory.gotItem(line, self)
      
  def connectionMade(self):
    basic.LineReceiver.connectionMade(self)
    self.client_address = self.transport.getPeer().host
    self.identifier = self.client_address
      
  def connectionLost(self, reason):
    self.logger.log(reason, logging.ERROR)
    self.factory.removeClient(self)
  

class BaseServerFactory(Factory):
  
  """
  Common server functionality
  Subclasses should override the buildProtocol method and attach the self.logger
  to it so the protocol can use logging
  """
  
  def __init__(self, spiderName, resultsModel, jobModel, logDir, shutdownOnZeroClients=False):
    # resultsModel and jobModel are Django model classes
    self._client_infos = []
    self.logger = ServerLogger(logDir)
    self.spider_name = spiderName
    self.results_model = resultsModel
    self.shutdown_on_zero_clients = shutdownOnZeroClients
    self.startup_command = self.getStartupCommand()
    self._createDownloadDirectory()
    self.job_model = jobModel
    self._initializeCrawling()
    self.logger.log("server started", logging.INFO)
        
  def _initializeCrawling(self):
    total_row_count = self.job_model.objects.count()
    non_crawled_count = self.job_model.objects.filter(crawled=False).count()
    if non_crawled_count == 0:
      self.logger.log("crawling finished last time. starting fresh", logging.INFO)
      self.logger.log("reinitializing tables. please wait...",
                  logging.INFO)
      self.job_model.objects.update(crawled=False)
      self.results_model.objects.all().delete()
      SpiderInfo.objects.reset_spider(self.spider_name, self.startup_command, self.logger.getLogFilePath())
      self.logger.log("reinitialization finished", logging.INFO)
    elif non_crawled_count == total_row_count:
      self.logger.log("looks like the first start of the spider", logging.INFO)
      SpiderInfo.objects.reset_spider(self.spider_name, self.startup_command, self.logger.getLogFilePath())
    elif non_crawled_count < total_row_count:
      self.logger.log("resuming crawling on {} jobs".format(non_crawled_count),
                    logging.INFO)
      SpiderInfo.objects.resume_spider(self.spider_name)
    
  def addClient(self, connection):
    new_client_info = {"connection":connection, "identifier":connection.identifier,
                        "returned_item_messages":[], "assigned_jobs":[]}
    self._client_infos.append(new_client_info)
    self.logger.log("client '{}' has been registered. current client count is {}"\
                  .format(new_client_info["identifier"], len(self._client_infos)), 
                  logging.INFO)
    
  def removeClient(self, connection):
    client_info = self._getClientInfoByConnection(connection)
    self._client_infos.remove(client_info)
    self.logger.log("client '{}' has disconnected. current client count is {}"\
                  .format(client_info["identifier"], len(self._client_infos)), logging.WARN)
    self.shutdown_if_no_clients()
    
  def setClientReady(self, connection):
    """
    called when a client has sent a ready signal
    if a client is ready and we have more ranges, we send it one
    if we don't have more ranges for the client, we tell it to shutdown_if_no_clients
    """
    client_info = self._getClientInfoByConnection(connection)
    self._insertClientRecords(client_info)
    self.logger.log("client '{}' is now ready".format(client_info["identifier"]),
                  logging.INFO)
    try:
      next_job_batch = self.job_model.objects.get_next_job()
    except StopIteration:
      # all IPs consumed
      self.shutdownClient(connection)
    else:
      client_info["assigned_jobs"] = next_job_batch
      client_info["connection"].sendLine(BaseServerToClientSignals.\
                                       makeJobMessage(next_job_batch))
      self.logger.log("range {} : {} sent to {}"\
                    .format(next_job_batch[0],
                     next_job_batch[-1], client_info["identifier"]), logging.INFO)
  
  def gotItem(self, itemLine, connection):
    client_info = self._getClientInfoByConnection(connection)
    client_info["returned_item_messages"].append(itemLine)
        
  def _insertClientRecords(self, clientInfo):
    returned_record_count = len(clientInfo["returned_item_messages"])
    self.logger.log("{} records returned from client '{}' [{} jobs were sent]. committing them..."\
                  .format(returned_record_count, clientInfo["identifier"],
                  len(clientInfo["assigned_jobs"])), logging.INFO)
    
    for record_line in clientInfo["returned_item_messages"]:
      new_record_dict = self.itemFromMessage(record_line)
      new_record_dict["client_count"] = len(self._client_infos)
      new_db_record = self.results_model(**new_record_dict)
      new_db_record.save()

    assigned_jobs = clientInfo.get("assigned_jobs")
    if assigned_jobs: 
      self.setJobsCrawled(assigned_jobs)
      
  def setJobsCrawled(self, assignedJobs):
    # a default implementation that can be overridden for custom jobs other than the list type
    for job in assignedJobs:
        self.job_model.objects.filter(job=job).update(crawled=True)
        
  def shutdown_if_no_clients(self):
    if self.shutdown_on_zero_clients and len(self._client_infos) == 0: # all clients shutdown_if_no_clients
      self.logger.log("server is shutting down. bye!", logging.INFO)
      reactor.stop() # shutdown_if_no_clients server too
        
  def shutdownClient(self, connection, reason="crawling finished"):
    client_info = self._getClientInfoByConnection(connection)
    client_info["connection"].sendLine(BaseServerToClientSignals.makeShutdownMessage())
    self.logger.log("shutdown signal sent to '{}'.Reason : {}".format(client_info["identifier"], 
      reason), logging.INFO)

  def _getClientInfoByConnection(self, connection):
    for client_info in self._client_infos:
      if client_info["identifier"] == connection.identifier:
        return client_info
    return {}
  
  def markClientReady(self, clientInfo):
    clientInfo["assigned_jobs"] = [] # that's a mark that the client is ready
    
  def isClientReady(self, clientInfo):
    return len(clientInfo["assigned_jobs"]) == 0
  
  def itemFromMessage(self, message):
    raise NotImplemented
  
  def getStartupCommand(self):
    # this method assumes the server class is located at the same file being run, which
    # may not be correct. Also, the placement of the method as belonging to the server
    # may be up for debate
    startup_command = sys.argv[:]
    startup_command[0] = path.abspath(__file__)
    startup_command.insert(0, sys.executable)
    return ' '.join(startup_command)
  
  def _createDownloadDirectory(self):
    output_file = getExportFileForSpider(self.spider_name)
    output_dir = path.dirname(output_file)
    if not path.exists(output_dir):
      os.mkdir(output_dir)
  
class InteractiveServerFactory(BaseServerFactory, SignalMixin):
  """
  A factory that honors signals to it's process
  Notice that clients are assumed always up and no special signals are
  sent to them when pausing or stopping.
  """
  
  def __init__(self, *args, **kwargs):
    BaseServerFactory.__init__(self, *args, **kwargs)
    SignalMixin.__init__(self, self)
    self.is_stopped = False 
    self.is_paused = False # checked when resuming from a temporary sleep
  
  def setClientReady(self, connection):
    if self.is_paused:
      # do not send new jobs to client
      client_info = self._getClientInfoByConnection(connection)
      self._insertClientRecords(client_info)
      self.markClientReady(client_info)
    elif self.is_stopped:
      client_info = self._getClientInfoByConnection(connection)
      self._insertClientRecords(client_info)
      self.shutdownClient(connection, reason="server asked to stop by user")
    else:
      BaseServerFactory.setClientReady(self, connection)
      
  def handlePauseOrResume(self):
    self.is_paused = not self.is_paused
    if self.is_paused:
      SpiderInfo.objects.pause_spider(self.spider_name, byUser=True)
    else:
      # pass jobs to all clients if they are ready 
      # (in case the resume happened before all clients were finished)
      SpiderInfo.objects.resume_spider(self.spider_name)
      for client_info in self._client_infos:
        if client_info["assigned_jobs"] == []:
          BaseServerFactory.setClientReady(self, client_info["connection"])
        
  def handleStop(self):
    self.shutdown_if_no_clients()
    self.is_stopped = True
    
class AutoSleepServerFactory(InteractiveServerFactory):
  
  """
  A server that sleeps periodically after a specific job count
  has be processed. This is to avoid getting banned by some servers
  The server will not sleep until all current clients has finished their jobs.
  """
  
  def __init__(self, jobThreshold=10000, minHours=1, maxHours=2, *args, **kwargs):
    """
    jobThreshold : The amount of jobs processed before the server will sleep
    """
    InteractiveServerFactory.__init__(self, *args, **kwargs)
    self.job_threshold = jobThreshold
    self.min_sleep_seconds = minHours * 60 * 60
    self.max_sleep_seconds = maxHours * 60 * 60
    self.assigned_jobs = 0
    
  def setClientReady(self, connection):
    if self.assigned_jobs <= self.job_threshold:
      InteractiveServerFactory.setClientReady(self, connection)
      client_info = self._getClientInfoByConnection(connection)
      self.assigned_jobs += len(client_info["assigned_jobs"])
    else:
      self.is_paused = True
      InteractiveServerFactory.setClientReady(self, connection)
      for client_info in self._client_infos:
        if not self.isClientReady(client_info):
          break
      else:
        self.enterSleepSequence()
        
  def enterSleepSequence(self):
    # all clients are ready. can sleep now
    sleep_time_seconds = randrange(self.min_sleep_seconds, self.max_sleep_seconds)
    sleep_time_mins = sleep_time_seconds / 60
    self.logger.log("going to sleep for {} minutes".format(sleep_time_mins), logging.INFO)
    SpiderInfo.objects.pause_spider(self.spider_name, byUser=False, timeToWakeup=sleep_time_seconds)
    sleep(sleep_time_seconds) # oO OO oOo
    self.is_paused = False
    self.assigned_jobs = 0
    SpiderInfo.objects.resume_spider(self.spider_name)
    for client_info in self._client_infos: # send jobs to clients
      InteractiveServerFactory.setClientReady(self, client_info["connection"])
      