# the user that runs the spiders should have write permissions to the
# download directory, or at least the user that runs the export script
DOWNLOAD_DIRECTORY = "/var/www/spidersite/downloads"