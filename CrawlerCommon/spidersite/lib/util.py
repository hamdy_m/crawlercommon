'''
Created on Dec 8, 2014
@author: Mohammed Hamdy
'''

import os, django, os.path as path, subprocess, tempfile

def setup_django():
  # necessary to use Django models without launching the app
  os.environ["DJANGO_SETTINGS_MODULE"] = "spidersite.spidersitex.settings"
  django.setup()
  
def getSiteLogDir():
  return path.join(path.dirname(path.dirname(__file__)), "logs")

def read_tail_lines(fpath, line_count=100):
  # this function is not cross platform. It uses the tail Command
  # to read from the end of fpath
  out = tempfile.TemporaryFile("w+")
  return_code = subprocess.call(["tail", "-{}".format(line_count), fpath], stdout=out)
  if return_code != 0: return ""
  out.seek(0)
  return out.read()