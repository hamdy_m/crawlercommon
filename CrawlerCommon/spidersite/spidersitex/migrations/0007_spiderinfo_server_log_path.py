# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spidersitex', '0006_whoisjobmodel_whoisresultsmodel'),
    ]

    operations = [
        migrations.AddField(
            model_name='spiderinfo',
            name='server_log_path',
            field=models.CharField(default=None, max_length=1024),
            preserve_default=False,
        ),
    ]
