# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spidersitex', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spiderinfo',
            name='stop_time',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
