# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spidersitex', '0005_auto_20141210_1833'),
    ]

    operations = [
        migrations.CreateModel(
            name='WhoisJobModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('range_start', models.GenericIPAddressField()),
                ('range_end', models.GenericIPAddressField()),
                ('crawled', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WhoisResultsModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('range_start', models.GenericIPAddressField()),
                ('range_end', models.GenericIPAddressField()),
                ('person', models.TextField()),
                ('company', models.TextField()),
                ('address', models.TextField()),
                ('zipcode', models.TextField()),
                ('city', models.TextField()),
                ('phone', models.TextField()),
                ('fax', models.TextField()),
                ('crawl_time', models.DateTimeField(auto_now=True)),
                ('client_count', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
