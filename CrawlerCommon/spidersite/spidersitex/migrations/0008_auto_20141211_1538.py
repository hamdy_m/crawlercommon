# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spidersitex', '0007_spiderinfo_server_log_path'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spiderinfo',
            name='server_log_path',
            field=models.CharField(max_length=1024, null=True),
            preserve_default=True,
        ),
    ]
