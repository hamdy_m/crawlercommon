# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spidersitex', '0004_wlwjobmodel_wlwresultsmodel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spiderinfo',
            name='name',
            field=models.CharField(unique=True, max_length=100),
            preserve_default=True,
        ),
    ]
