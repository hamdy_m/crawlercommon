# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spidersitex', '0002_auto_20141209_1734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spiderinfo',
            name='average_hour',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='spiderinfo',
            name='average_hour_client',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='spiderinfo',
            name='average_last_hour_client',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='spiderinfo',
            name='cumulative_sleep',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='spiderinfo',
            name='last_hour_count',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='spiderinfo',
            name='until_wakeup',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
    ]
