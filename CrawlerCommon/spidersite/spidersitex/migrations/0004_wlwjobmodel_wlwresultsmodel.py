# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spidersitex', '0003_auto_20141209_1739'),
    ]

    operations = [
        migrations.CreateModel(
            name='WLWJobModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('job', models.IntegerField(db_index=True)),
                ('crawled', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WLWResultsModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company', models.TextField()),
                ('address', models.TextField()),
                ('email', models.TextField()),
                ('fax', models.TextField()),
                ('phone', models.TextField()),
                ('website', models.TextField()),
                ('suffix', models.IntegerField()),
                ('crawl_time', models.DateTimeField(auto_now=True)),
                ('client_count', models.IntegerField(default=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
