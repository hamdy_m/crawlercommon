# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FirmenJobModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('job', models.CharField(max_length=100, db_index=True)),
                ('crawled', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FirmenResultsModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company', models.CharField(max_length=255)),
                ('website', models.CharField(max_length=255)),
                ('city', models.CharField(max_length=255)),
                ('zipcode', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=255)),
                ('house_number', models.CharField(max_length=255)),
                ('street', models.CharField(max_length=255)),
                ('sub', models.CharField(max_length=255)),
                ('client_count', models.IntegerField()),
                ('crawl_time', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SpiderInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('startup_command', models.CharField(max_length=255)),
                ('process_id', models.IntegerField()),
                ('record_count', models.IntegerField()),
                ('start_time', models.DateTimeField()),
                ('stop_time', models.DateTimeField()),
                ('cumulative_sleep', models.IntegerField()),
                ('until_wakeup', models.IntegerField()),
                ('last_hour_count', models.IntegerField()),
                ('average_hour', models.FloatField()),
                ('average_hour_client', models.FloatField()),
                ('average_last_hour_client', models.FloatField()),
                ('state', models.CharField(max_length=2, choices=[(b'R+', b'Running'), (b'P+', b'User-paused'), (b'P-', b'Self-paused'), (b'SP', b'Stop pending'), (b'S', b'Stopped')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
