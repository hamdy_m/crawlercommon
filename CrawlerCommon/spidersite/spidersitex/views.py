'''
Created on Nov 12, 2014
@author: Mohammed Hamdy
'''
from __future__ import division
import os.path as path
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.utils.encoding import smart_str
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.views.decorators.http import require_POST
from django.contrib import messages
from .models import SpiderInfo
from spidersite.lib.util import read_tail_lines
from crawlercommon.lib.util import getExportFileForSpider, timeFromSeconds, populate_messages, run_in_thread
from crawlercommon.lib.signal_manager import SpiderController
from crawlercommon.lib.digitalk import cloneReadyClient, rebootAllClients, destroyAllClients, getClientCount
from .forms import CloneForm


def home(request):
  return render_to_response("home.html")

@login_required
def download(request, id):
  spider_record = get_object_or_404(SpiderInfo, pk=id)
  spider_name = spider_record.name
  response = HttpResponse(content_type="text/csv")
  response["Content-Disposition"] = "attachment; filename={}_records.csv".format(spider_name)
  response["X-Accel-Redirect"] = smart_str(getExportFileForSpider(spider_name, "/crawler-downloads"))
  response["Content-Length"] = path.getsize(getExportFileForSpider(spider_name))
  return response
  
@login_required
def spider_list(request):
  spider_records = SpiderInfo.objects.all()
  return render_to_response("list_spiders.html", {"spiders":spider_records}, 
                            context_instance=RequestContext(request))

@login_required
def spider_details(request, id):
  spider_record = get_object_or_404(SpiderInfo, pk=id)
  spider_running_time_seconds = (timezone.now() - spider_record.start_time).total_seconds()\
    - spider_record.cumulative_sleep
  hours, mins, seconds = timeFromSeconds(spider_running_time_seconds)
  if spider_record.state == "P-":
    # self paused state
    time_passed_secs = (timezone.now() - spider_record.stop_time).total_seconds()
    remaining_mins = (spider_record.until_wakeup - time_passed_secs) / 60
  else:
    remaining_mins = 0
  context = {"record_count":spider_record.record_count, "record_count_last_hour":spider_record.last_hour_count,
          "time_taken_hours":hours, "time_taken_mins":mins, "time_taken_seconds":seconds,
          "average_hour":spider_record.average_hour, "average_hour_client":spider_record.average_hour_client,
          "average_last_hour_client":spider_record.average_last_hour_client, "spider_name":spider_record.name,
          "spider_state":spider_record.get_state_display(),
          "spider_id":spider_record.id, "time_to_wakeup_mins":remaining_mins, 
          "server_log":read_tail_lines(spider_record.server_log_path)}
  return render_to_response("spider_detail.html", context, context_instance=RequestContext(request))

@login_required
@require_POST
def spider_start(request, id):
  spider_record = get_object_or_404(SpiderInfo, pk=id)
  controller = SpiderController(request.user.username)
  return_code = controller.start_spider(id)
  populate_messages(request, "start", return_code)
  return HttpResponseRedirect(reverse("spider_details", args=(id,)))

@login_required
@require_POST
def spider_pause(request, id):
  spider_record = get_object_or_404(SpiderInfo, pk=id)
  controller = SpiderController(request.user.username)
  return_code = controller.pause_spider(id)
  populate_messages(request, "pause", return_code)
  return HttpResponseRedirect(reverse("spider_details", args=(id,)))
  
@login_required
@require_POST
def spider_resume(request, id):
  spider_record = get_object_or_404(SpiderInfo, pk=id)
  controller = SpiderController(request.user.username)
  return_code = controller.resume_spider(id)
  populate_messages(request, "resume", return_code)
  return HttpResponseRedirect(reverse("spider_details", args=(id,)))

@login_required
@require_POST
def spider_shutdown(request, id):
  spider_record = get_object_or_404(SpiderInfo, pk=id)
  controller = SpiderController(request.user.username)
  return_code = controller.shutdown_spider(id)
  populate_messages(request, "shutdown", return_code)
  return HttpResponseRedirect(reverse("spider_details", args=(id,)))

@login_required
def digitalocean_view(request):
  clone_count = getClientCount()
  form = CloneForm()
  return render_to_response("digitalocean.html", {"form":form, "clone_count":clone_count},
                            context_instance=RequestContext(request))

@login_required
@require_POST
def clone_clients(request):
  clone_form = CloneForm(request.POST)
  if clone_form.is_valid():
    # launch a clone thread so not to block the view and probably django
    run_in_thread(cloneReadyClient, clone_form.cleaned_data["clone_count"])
    messages.info(request, 'Cloning in progress. Check <a target="_blank" href="http://www.digitalocean.com">digitalocean website</a> for more')
    return HttpResponseRedirect(reverse("digitalocean_options"))

@login_required
def reboot_clients(request):
  run_in_thread(rebootAllClients)
  messages.info(request, 'Rebooting in progress. Check <a target="_blank" href=http://"www.digitalocean.com">digitalocean website</a> for more')
  return HttpResponseRedirect(reverse("digitalocean_options"))

@login_required
def destroy_clients(request):
  run_in_thread(destroyAllClients)
  messages.info(request, 'Destruction in progress. Check <a target="_blank" href="http://www.digitalocean.com">digitalocean website</a> for more')
  return HttpResponseRedirect(reverse("digitalocean_options"))

