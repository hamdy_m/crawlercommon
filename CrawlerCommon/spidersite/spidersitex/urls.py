from django.conf.urls import patterns, url, include
from django.conf.urls.static import static
from django.contrib.auth.views import login as django_login_view
from django.contrib.auth.views import logout_then_login
from django.conf import settings
from spidersite.spidersitex import views

user_patterns = patterns('',
  url("login", django_login_view, name="login"),
  url("logout", logout_then_login, name="logout"))

spider_patterns = patterns('',
    url(r"^list/$", views.spider_list, name="spider_list"),
    url(r"^(?P<id>\d+)/details/$", views.spider_details, name="spider_details"),
    url(r"^(?P<id>\d+)/download/$", views.download, name="spider_download"),
    url(r"^(?P<id>\d+)/start/$", views.spider_start, name="spider_start"),
    url(r"^(?P<id>\d+)/pause/$", views.spider_pause, name="spider_pause"),
    url(r"^(?P<id>\d+)/resume/$", views.spider_resume, name="spider_resume"),
    url(r"^(?P<id>\d+)/shutdown/$", views.spider_shutdown, name="spider_shutdown")
    )

client_patterns = patterns('',
    url(r"reboot/$", views.reboot_clients, name="reboot_clients"),
    url(r"destroy/$", views.destroy_clients, name="destroy_clients"),
    url(r"clone/$", views.clone_clients, name="clone_clients")                      
    )

urlpatterns = patterns('',
    url("^$", views.home, name="home"),
    url("^spiders/", include(spider_patterns)),
    url("^users/", include(user_patterns)),
    url("^clients/", include(client_patterns)),
    url(r"^digitalocean/$", views.digitalocean_view, name="digitalocean_options")
    #url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)