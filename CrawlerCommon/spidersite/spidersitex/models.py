'''
Created on Dec 6, 2014
@author: Mohammed Hamdy
'''

from django.db import models
import unicodecsv
from django.utils import timezone
from django.db.models import Avg
from django.core.urlresolvers import reverse
from crawlercommon.lib.util import timeFromSeconds, getExportFileForSpider
import os


class SpiderInfoManager(models.Manager):
  
  """
  A manager with some helper methods to update spider state
  """
  
  def spider_exists(self, spiderName):
    try:
      self.get(name=spiderName)
      return True
    except self.model().DoesNotExist:
      return False
  
  def reset_spider(self, spiderName, startupCommand, logFilePath):
    # this resets the start time and all counters for the spider
    start_values = {"start_time":timezone.now(),
    "state":self.model().STATE_RUNNING, "until_wakeup":0,
    "cumulative_sleep":0, "record_count":0,
    "process_id":os.getpid(), "startup_command":startupCommand,
    "server_log_path":logFilePath}
    try: # because the get_or_create method is mad
      spider_record = self.get(name=spiderName)
      [setattr(spider_record, field_name, field_value) for field_name, field_value 
       in start_values.iteritems()]
      spider_record.save()
    except self.model().DoesNotExist:
      self.model(name=spiderName, **start_values).save()
    
  def pause_spider(self, spiderName, byUser=False, timeToWakeup=0):
    spider_record = self.get(name=spiderName)
    if byUser:
      spider_record.state = self.model().STATE_USER_PAUSED
    else:
      spider_record.state = self.model().STATE_SELF_PAUSED
      spider_record.until_wakeup = timeToWakeup
    spider_record.stop_time = timezone.now()
    spider_record.save()
      
  def resume_spider(self, spiderName):
    # can be called after a stop or a paused
    spider_record = self.get(name=spiderName)
    if spider_record.stop_time is None:
      # the spider has been inorderly shutdown and didn't have a chance to update
      # it's stop time (stop_spider() method). We'll use a half hour flat penalty
      spider_record.stop_time = timezone.now() - timezone.timedelta(minutes=30)
    time_slept = (timezone.now() - spider_record.stop_time).total_seconds()
    spider_record.cumulative_sleep += time_slept
    spider_record.state = self.model().STATE_RUNNING
    spider_record.process_id = os.getpid()
    spider_record.save()
    
  def stop_spider(self, spiderName):
    spider_record = self.get(name=spiderName)
    spider_record.state = self.model().STATE_STOPPED
    spider_record.stop_time = timezone.now()
    spider_record.save()

class SpiderInfo(models.Model):
  
  class Meta:
    app_label = "spidersitex"
  
  name = models.CharField(max_length=100, unique=True)
  startup_command = models.CharField(max_length=255)
  server_log_path = models.CharField(max_length=1024, null=True)
  process_id = models.IntegerField()
  record_count = models.IntegerField()
  start_time = models.DateTimeField()
  stop_time = models.DateTimeField(null=True)
  cumulative_sleep = models.IntegerField(null=True) # the count of seconds slept
  until_wakeup = models.IntegerField(null=True)     # the count of seconds until the spider wakes up from sleep
  last_hour_count = models.IntegerField(null=True)
  average_hour = models.FloatField(null=True)
  average_hour_client = models.FloatField(null=True)
  average_last_hour_client = models.FloatField(null=True)
  
  STATE_RUNNING = "R+"
  STATE_USER_PAUSED = "P+"
  STATE_SELF_PAUSED = "P-"
  STATE_STOP_PENDING = "SP"
  STATE_STOPPED = "S"
  
  SPIDER_STATES = [
                   (STATE_RUNNING, "Running"),
                   (STATE_USER_PAUSED, "User-paused"),
                   (STATE_SELF_PAUSED, "Self-paused"),
                   (STATE_STOP_PENDING, "Stop pending"),
                   (STATE_STOPPED, "Stopped")
                  ]
  
  state = models.CharField(max_length=2, choices=SPIDER_STATES)
  
  objects = SpiderInfoManager()
  
  def get_absolute_url(self):
    return reverse("spider_detail", args=[str(self.id)])
  
  def __repr__(self):
    return "<SpiderInfo {}>".format(self.name)

class ExportManager(models.Manager):
  """
  A manager that adds exporting spider info capability to the manager's 
  subclasses
  """
  
  def __init__(self, spiderName):
    # spiderName must match a value in the name column of SpierInfo model
    # this name is registered by the spider server when it starts
    super(ExportManager, self).__init__()
    self.spider_name = spiderName
  
  def update_stats(self):
    queryset = super(ExportManager, self).get_queryset()
    record_count = queryset.all().count()
    time_taken_seconds = self.get_spider_run_time()
    if time_taken_seconds == 0: return # nothing to report
    hours, mins, seconds = timeFromSeconds(time_taken_seconds)
    average_client_count = queryset.all().aggregate(Avg("client_count")).pop("client_count__avg")
    average_client_count = self.default_client_count(average_client_count)
    records_per_hour = record_count / (hours + (mins / 60))
    avg_hour_client = records_per_hour / average_client_count
    last_hour_time = timezone.now() - timezone.timedelta(hours=1)
    record_count_last_hour = queryset.all().filter(crawl_time__gt=last_hour_time).count()
    average_client_count_last_hour = queryset.all().filter(crawl_time__gt=last_hour_time)\
      .aggregate(Avg("client_count")).pop("client_count__avg")
    average_client_count_last_hour = self.default_client_count(average_client_count_last_hour)
    avg_last_hour_per_client = record_count_last_hour / average_client_count_last_hour
    results = {"record_count":record_count, "average_hour":records_per_hour, 
               "average_hour_client":avg_hour_client, "last_hour_count":record_count_last_hour,
               "average_last_hour_client":avg_last_hour_per_client}
    SpiderInfo.objects.filter(name=self.spider_name).update(**results)
    
  def export_data(self):
    # dumps all the table data (except the id column) to the specified file in CSV format
    output_file = getExportFileForSpider(self.spider_name)
    queryset = super(ExportManager, self).get_queryset()
    row_dicts = queryset.values()
    with open(output_file, "wb") as csv_output:
      if not row_dicts: return
      field_names = row_dicts[0].keys()
      writer = unicodecsv.DictWriter(csv_output, fieldnames=field_names, delimiter=',',
                                   extrasaction="ignore", encoding="utf-8")
      writer.writeheader()
      for row_dict in row_dicts:
        row_dict.pop("id")
        writer.writerow(row_dict)
    
  def get_spider_run_time(self):
    # returns the spider total running time in seconds
    try:
      info_record = SpiderInfo.objects.get(name=self.spider_name)
    except SpiderInfo.DoesNotExist:
      return 0
    else:
      start_time = info_record.start_time
      time_taken = timezone.now() - start_time # this is a timedelta object
      time_taken_seconds = time_taken.total_seconds()
      time_taken_seconds = time_taken_seconds - info_record.cumulative_sleep
      return time_taken_seconds
  
  def default_client_count(self, client_count):
    return 1 if client_count is None else client_count
  
class FirmenResultsModel(models.Model):
  
  class Meta:
    app_label = "spidersitex"
  
  company = models.CharField(max_length=255)
  website = models.CharField(max_length=255)
  city = models.CharField(max_length=255)
  zipcode = models.CharField(max_length=255)
  phone = models.CharField(max_length=255)
  house_number = models.CharField(max_length=255)
  street = models.CharField(max_length=255)
  sub = models.CharField(max_length=255)
  client_count = models.IntegerField()
  crawl_time = models.DateTimeField(auto_now=True)
  
  objects = ExportManager("FirmenCrawler")
  
  
class JobManager(models.Manager):
  """
  A manager that handles generating jobs to the server. It expects 2 fields
  on the model to be able to work correctly:
  1- job   2- crawled
  """
  def __init__(self, jobSize=1000):
    models.Manager.__init__(self)
    self.job_size = jobSize
    self.last_served_index = 0
    
  def get_next_job(self):
    # returns a job that's usually passed to a signal maker or raises StopIteration if no more jobs
    # exist
    queryset = models.Manager.get_queryset(self)
    non_crawled = queryset.filter(crawled=False).all()
    non_crawled_count = len(non_crawled)
    while non_crawled_count > 0:
      next_job = non_crawled[self.last_served_index:self.last_served_index+self.job_size]
      if len(next_job) == 0:
        non_crawled = queryset.filter(crawled=False).all()
        non_crawled_count = len(non_crawled)
        if len(non_crawled_count) == 0:
          raise StopIteration
        self.last_served_index = 0
      else:
        self.last_served_index += self.job_size
        return [o.job for o in next_job]
    raise StopIteration # no jobs from the start
      
class FirmenJobManager(JobManager):
  
  def init_model(self, zipcodesCSVPath):
    # fills the model from the csv plz column
    queryset = JobManager.get_queryset(self)
    queryset.all().delete()
    with open(zipcodesCSVPath, "rb") as zipcodes_file:
      csv_reader = unicodecsv.DictReader(zipcodes_file)
      urlsub_entries = []
      for i, row in enumerate(csv_reader):
        urlsub_entries.append(FirmenJobModel(job=row["plz"], crawled=False))
      queryset.bulk_create(urlsub_entries)
      print("Wrote {} rows to job table".format(i))

class FirmenJobModel(models.Model):
  
  class Meta:
    app_label = "spidersitex"
  
  job = models.CharField(max_length=100, db_index=True) # job here is a url query number
  crawled = models.BooleanField(default=False)
  
  objects = FirmenJobManager(jobSize=10)
  

class WLWResultsModel(models.Model):
  
  class Meta:
    app_label = "spidersitex"
    
  company = models.TextField()
  address = models.TextField()
  email = models.TextField()
  fax = models.TextField()
  phone = models.TextField()
  website = models.TextField()
  suffix = models.IntegerField()
  crawl_time = models.DateTimeField(auto_now=True)
  client_count = models.IntegerField(default=1)
  
  objects = ExportManager("WLWCrawler")
  
  
class WLWJobManager(JobManager):
  
  def init_model(self, min_suffix, max_suffix, suffix_list=[]):
    # fills the model from suffix_list. if that's empty, then from min_suffix:max_suffix
    queryset = JobManager.get_queryset(self)
    queryset.all().delete()
    if len(suffix_list) == 0:
      for i in range(min_suffix, max_suffix + 1):
        job_object = self.model(job=i, crawled=False)
        job_object.save()
        if i % 10000 == 0:
          print("{}/{}".format(i, max_suffix))
    else:
      queryset.bulk_create([self.model(job=i, crawled=False) for i in suffix_list])
  
class WLWJobModel(models.Model):
  
  class Meta:
    app_label = "spidersitex"
  
  job = models.IntegerField(db_index=True)
  crawled = models.BooleanField(default=False)
  
  objects = WLWJobManager(jobSize=1000)
  
class WhoisResultsModel(models.Model):
  
  class Meta:
    app_label = "spidersitex"
    
  range_start = models.GenericIPAddressField()
  range_end = models.GenericIPAddressField()
  person = models.TextField()
  company = models.TextField()
  address = models.TextField()
  zipcode = models.TextField()
  city = models.TextField()
  phone = models.TextField()
  fax = models.TextField()
  crawl_time = models.DateTimeField(auto_now=True)
  client_count = models.IntegerField()
    
  objects = ExportManager("WhoisCrawler")
  
  
class WhoisJobManager(JobManager):
  
  def get_next_job(self):
    queryset = JobManager.get_queryset(self)
    try:
      next_non_crawled = queryset.filter(crawled=False)[0]
    except IndexError:
      raise StopIteration
    else:
      return next_non_crawled.range_start, next_non_crawled.range_end
    
  def init_model(self, germany_csv_path):
    # initializes the job model from the csv file
    queryset = JobManager.get_queryset(self)
    row_count = 0
    with open(germany_csv_path) as maxmind_germany:
      reader = unicodecsv.reader(maxmind_germany)
      queryset.all().delete()
      for company_row in reader:
        self.model(range_start=company_row[0], range_end=company_row[1], crawled=False).save()
        row_count += 1
      print("Successfully inserted {} rows\n".format(row_count))
    
  
class WhoisJobModel(models.Model):
  
  class Meta:
    app_label = "spidersitex"
    
  range_start = models.GenericIPAddressField()
  range_end = models.GenericIPAddressField()
  crawled = models.BooleanField(default=False)
  
  objects = WhoisJobManager(jobSize=1)
  