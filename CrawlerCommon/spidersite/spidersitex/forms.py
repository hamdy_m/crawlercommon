'''
Created on Dec 11, 2014
@author: Mohammed Hamdy
'''

from crawlercommon.lib.digitalk import READY_CLIENT_NAME

from django import forms

class CloneForm(forms.Form):
  
  clone_count = forms.IntegerField(label="Clone Count (excludes {})".format(READY_CLIENT_NAME))
  
